from flask import jsonify
from flask_restful import Resource
from config import logger


class SmokeResourse(Resource):
    """ Resource for basic application smoke test """
    def get(self):
        logger.info("Smoke")
        return jsonify({"message": "Hello, World!"})
