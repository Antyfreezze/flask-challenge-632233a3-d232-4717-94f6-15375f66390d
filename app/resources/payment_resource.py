from flask import make_response, render_template, request, flash
from flask_restful import Resource

from app.domain.payment_handler import PaymentHandler


class PaymentResource(Resource):
    """ Resource for basic application smoke test """
    def get(self):
        return make_response(render_template("payment.html"))
    
    def post(self):
        payment_method = getattr(PaymentHandler, request.form["currency"])
        try:
            template_attrs, form_attrs = payment_method(**request.form)
        except ValueError as e:
            flash(e)
            return make_response(render_template("payment.html"))
        rendered_template = render_template(
            "payment_form.html", template_attrs=template_attrs, form_attrs=form_attrs
        )
        return make_response(rendered_template)

