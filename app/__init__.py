import logging.config
from flask import Flask
from app.routes import register_routes


def create_app(config_obj=None):
    app = Flask(__name__)

    if not config_obj:
        logging.warning(
            "No config specified; defaulting to development"
        )
        import config
        config_obj = config.DevelopmentConfig

    app.config.from_object(config_obj)

    register_routes(app)
    return app
