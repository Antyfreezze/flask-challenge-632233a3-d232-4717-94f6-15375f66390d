from flask import Blueprint
from flask_restful import Api
from app.resources.smoke_resource import SmokeResourse
from app.resources.payment_resource import PaymentResource


def register_routes(app):
    api_blueprint = Blueprint("api", __name__)
    api = Api(api_blueprint)

    api.add_resource(
        SmokeResourse, "/smoke", strict_slashes=False
    )
    api.add_resource(
        PaymentResource, "/payment", strict_slashes=False
    )

    app.register_blueprint(api_blueprint, url_prefix="/api")
