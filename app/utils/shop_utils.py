import uuid
from datetime import datetime

from config import logger


def get_order_id():
    return uuid.uuid4()


def audit_logs(**kwargs):
    logs = {
        "time": datetime.now().strftime("[%d%b%Y %H:%M:%S]"),
        "level": "AUDIT",
        "order_id": kwargs["shop_order_id"],
        "currency": kwargs["currency"],
        "amount": kwargs["amount"],
        "descriprion": kwargs.get("description", "")
    }
    logger.info(logs)
