import json
import requests

from copy import deepcopy
from collections import OrderedDict
from hashlib import sha256
from os import environ as env

from app.utils.shop_utils import get_order_id, audit_logs
from config import logger


class PaymentHandler:
    _mapping = {
        "RUB": {"num": "643", "url": "https://core.piastrix.com/invoice/create"},
        "EUR": {"num": "978", "url": "https://pay.piastrix.com/ru/pay"},
        "USD": {"num": "978", "url": "https://core.piastrix.com/bill/create"}
    }
    
    @classmethod
    def EUR(cls, **kwargs):
        context_mapping = cls._mapping[kwargs["currency"]]
    
        base_dict = OrderedDict({
            "amount": kwargs["amount"],
            "currency": context_mapping["num"],
            "shop_id": env["SHOP_ID"]
        })

        signature_args = deepcopy(base_dict)
        order_id = str(get_order_id())
        signature_args.update({"shop_order_id": order_id})
        audit_logs(**kwargs, shop_order_id=order_id)

        sign = cls._generate_signature(signature_args)
        signature_args.update({"sign": sign})

        form_attrs = {
            "url": context_mapping["url"],
            "method": "post",
            "input_type": "hidden",
            "name": "Pay"
        }
        return signature_args, form_attrs

    @classmethod
    def USD(cls, **kwargs):
        context_mapping = cls._mapping[kwargs["currency"]]
        base_dict = OrderedDict({
            "payer_currency": context_mapping["num"],
            "shop_amount": kwargs["amount"],
            "shop_currency": context_mapping["num"],
            "shop_id": str(env["SHOP_ID"]),
            "shop_order_id": str(get_order_id()),
        })
        audit_logs(**kwargs, **base_dict)

        sign = cls._generate_signature(base_dict)
        base_dict.update({"sign": sign})

        response = requests.post(
            context_mapping["url"],
            data=json.dumps(base_dict),
            headers={"Content-Type": "application/json"}
        )
        response_data = response.json()
        if response_data["error_code"]:
            logger.error(response_data)
            raise ValueError(response_data)

        form_attrs = {
            "url": response_data["data"]["url"],
            "method": "get"
        }
        return {}, form_attrs

    @classmethod
    def RUB(cls, **kwargs):
        context_mapping = cls._mapping[kwargs["currency"]]
        order_id = str(get_order_id())
        base_dict = OrderedDict({
            "amount": kwargs["amount"],
            "currency": context_mapping["num"],
            "payway": "payeer_rub",
            "shop_id": str(env["SHOP_ID"]),
            "shop_order_id": order_id,
        })
        audit_logs(**kwargs, shop_order_id=order_id)

        sign = cls._generate_signature(base_dict)
        base_dict.update({"sign": sign})

        response = requests.post(
            context_mapping["url"],
            data=json.dumps(base_dict),
            headers={"Content-Type": "application/json"}
        )
        response_data = response.json()
        form_attrs = {
            "url": response_data["data"]["url"],
            "method": response_data["data"]["method"],
            "input_type": "hidden"
        }
        return response_data["data"]["data"], form_attrs

    @classmethod
    def _generate_signature(cls, sign_attrs: OrderedDict):
        signature = ":".join(sign_attrs.values()) + env["SECRET_KEY"]
        encrypted_signature = sha256(signature.encode()).hexdigest()
        return encrypted_signature

