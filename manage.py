import click

from app import create_app


@click.group()
def cli():
    pass


@click.command()
def runserver():
    app = create_app()
    app.run()


cli.add_command(runserver)


if __name__ == "__main__":
    cli()
