""" Config file for Flask app """
import logging


logging.basicConfig(level=logging.INFO, filename="logs.log")
logger = logging.getLogger("app")


class DevelopmentConfig:
    HOST = "localhost"
    PORT = "5000"
    DEBUG = True
    SECRET_KEY = "some_really_secret_key"
    FLASK_ENV = "development"
